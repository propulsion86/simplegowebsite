package main

import (
	"net/http"
	"log"
	"fmt"
	"database/sql"
	"os"
	"encoding/json"

	_ "github.com/mattn/go-sqlite3"
)

func main() {
	db, err := sql.Open("sqlite3", "./users.db")

	var logged string = ""

	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	if err := createTableIfNotExisting(db); err != nil {
		log.Fatal(err)
	}

	http.Handle("/", http.FileServer(http.Dir("./public")))

	fs := http.FileServer(http.Dir("public"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))

	http.HandleFunc("/login", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "./public/login.html")
	})

	http.HandleFunc("/submit", func(w http.ResponseWriter, r *http.Request) {
		err := r.ParseForm()
		if err != nil {
			http.Error(w, "Error parsing the form", http.StatusInternalServerError)
			return
		}

		username := r.FormValue("username")
		password := r.FormValue("password")
		fmt.Printf("Recieved: Username = %s, Password = %s\n", username, password)

		if !checkPassword(username, password, db){
			http.Redirect(w, r, "/login", http.StatusSeeOther)
		}else{
			logged = username
			http.Redirect(w, r, "/next-page", http.StatusSeeOther)
		}
	})

	http.HandleFunc("/signup", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "./public/signup.html")
	})

	http.HandleFunc("/next-page", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "./public/next-page.html")
	})

	http.HandleFunc("/signout", func(w http.ResponseWriter, r *http.Request) {
		logged = ""
		http.Redirect(w, r, "/", http.StatusSeeOther)
	})

	http.HandleFunc("/adduser", func(w http.ResponseWriter, r * http.Request) {
		err := r.ParseForm()
		if err != nil {
			http.Error(w, "Error parsing the form", http.StatusInternalServerError)
		}

		username := r.FormValue("username")
		password := r.FormValue("password")

		if username == "" {
			http.Redirect(w, r, "/?msg=username-can-not-be-empty", http.StatusSeeOther)

		}

		if password == "" {
			http.Redirect(w, r, "/?msg=password-can-not-be-empty", http.StatusSeeOther)

		}

		fmt.Println(username)
		fmt.Println(password)

		err = adduser(username, password, db)

		if err != nil {
			http.Error(w, "Error adding User", http.StatusInternalServerError)
			fmt.Println(err)
			return
		}

		http.Redirect(w, r, "/login", http.StatusSeeOther)
	})

	http.HandleFunc("/allimages", func(w http.ResponseWriter, r *http.Request) {
		if logged != "" {
			http.ServeFile(w, r, "./public/allimages.html")
		}else{
			http.Redirect(w, r, "/", http.StatusSeeOther)
		}

	})

	http.HandleFunc("/images", func(w http.ResponseWriter, r *http.Request) {
		files, err := os.ReadDir("public/img") // Changed from ioutil.ReadDir to os.ReadDir
		if err != nil {
			http.Error(w, "Failed to read directory", http.StatusInternalServerError)
			return
		}

		var imageFiles []string
		for _, file := range files {
			if !file.IsDir() {
				// To get the file name, now you need to use file.Name()
				imageFiles = append(imageFiles, file.Name())
			}
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(imageFiles)
	})

	log.Println("Server starting on http://localhost:8080/")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func checkPassword(uname string, passw string, db *sql.DB) bool {
	query := "SELECT password FROM users WHERE username = ?"
	var pword string

	err := db.QueryRow(query, uname).Scan(&pword)

	if err != nil {
		if err == sql.ErrNoRows {
			return false
		}
	}

	if pword == passw {
		return true
	}

	return false
}

func adduser(uname string, passw string, db *sql.DB) error {
	query := "INSERT INTO users (username, password) VALUES (?, ?)"

	result, err := db.Exec(query, uname, passw)

	fmt.Println(result)

	if err != nil {
		return err
	}

	return nil
}

func createTableIfNotExisting(db *sql.DB) error {
	query := `
	CREATE TABLE IF NOT EXISTS users (
		username TEXT PRIMARY KEY,
		password TEXT NOT NULL
		);
		`

		_, err := db.Exec(query)
		return err
}
